import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import axios from "axios";

const UsersScreen = () => {
  const navigation = useNavigation();

  const [users, setUsers] = useState([]);
  useEffect(() => {
    getUsers();
  }, []);

  async function getUsers() {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      setUsers(response.data);
    } catch (error) {
      console.error(error);
    }
  }

  const taskPress = (user) => {
    navigation.navigate("Details", { user });
  };

  const renderItem = ({ item, index }) => {
    const backgroundColor = index % 2 === 0 ? "teal" : "lightblue";
    const textColor = index % 2 === 0 ? "white" : "black";

    return (
      <TouchableOpacity
        style={[styles.item, { backgroundColor }]}
        onPress={() => taskPress(item)}
      >
        <Text style={[styles.text, { color: textColor }]}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.text2}>Lista de Usuários</Text>
      <FlatList
        data={users}
        keyExtractor={(item) => item.id.toString()}
        renderItem={renderItem}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  item: {
    padding: 10,
    marginBottom: 5,
    alignItems: "center",
    justifyContent: "center",
    width: 500,
  },
  text: {
    fontSize: 16,
  },
  text2: {
    fontSize: 20,
    fontWeight: "bold",
  },
});

export default UsersScreen;
