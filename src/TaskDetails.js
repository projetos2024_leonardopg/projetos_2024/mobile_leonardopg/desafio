import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import axios from "axios";

const TaskDetails = ({ route }) => {
  const { user } = route.params;

  return (
    <View style={styles.container}>
      <Text style={[styles.text, styles.header]}>Informações dos Usuários</Text>
      <View>
        <Text style={[styles.text, styles.teal]}>ID: {user.id}</Text>
        <Text style={[styles.text, styles.lightblue]}>Name: {user.name}</Text>
        <Text style={[styles.text, styles.teal]}>
          Username: {user.username}
        </Text>
        <Text style={[styles.text, styles.lightblue]}>Email: {user.email}</Text>
        <Text style={[styles.text, styles.teal]}>
          Address: {user.address.street}, {user.address.suite},{" "}
          {user.address.city}, {user.address.zipcode}
        </Text>
        <Text style={[styles.text, styles.lightblue]}>
          Latitude: {user.address.geo.lat}
        </Text>
        <Text style={[styles.text, styles.teal]}>
          Longitude: {user.address.geo.lng}
        </Text>
        <Text style={[styles.text, styles.lightblue]}>Phone: {user.phone}</Text>
        <Text style={[styles.text, styles.teal]}>Website: {user.website}</Text>
        <Text style={[styles.text, styles.lightblue]}>
          Company Name: {user.company.name}
        </Text>
        <Text style={[styles.text, styles.teal]}>
          Catch Phrase: {user.company.catchPhrase}
        </Text>
        <Text style={[styles.text, styles.lightblue]}>
          Business: {user.company.bs}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    padding: 10,
    marginBottom: 5,
  },
  header: {
    fontSize: 20,
    fontWeight: "bold",
  },
  teal: {
    backgroundColor: "teal",
    color: "white",
  },
  lightblue: {
    backgroundColor: "lightblue",
    color: "black",
  },
});

export default TaskDetails;
