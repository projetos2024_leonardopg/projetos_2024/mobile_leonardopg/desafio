import React from "react";
import { View, Button, StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

const HomeScreen = () => {
  const navigation = useNavigation();

  const handleUsers = () => {
    navigation.navigate("Users");
  };

  return (
    <View style={styles.fundo}>
        <TouchableOpacity style={styles.item} onPress={handleUsers}>Ver Usuários</TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  item: {
    padding: 10,
    marginBottom: 5,
    alignItems: "center",
    justifyContent: "center",
    width: 500,
    backgroundColor: "skyblue",
    fontSize: 20,
    fontWeight: "bold",
  },
  fundo: {
    alignItems: "center",
    justifyContent: "center",
    margin: 50,
  },
});
export default HomeScreen;
