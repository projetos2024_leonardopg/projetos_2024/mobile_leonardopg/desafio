import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import UsersScreen from "./src/usersScreen";
import HomeScreen from "./src/homeScreen";
import TaskDetails from "./src/TaskDetails";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen}></Stack.Screen>
        <Stack.Screen name="Users" component={UsersScreen}></Stack.Screen>
        <Stack.Screen name="Details" component={TaskDetails}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}